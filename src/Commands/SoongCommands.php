<?php

namespace Drupal\soong\Commands;

use Drupal\soong\YamlRecursiveFilterIterator;
use Drush\Commands\DrushCommands;
use Soong\Task\Task;
use Symfony\Component\Yaml\Yaml;

/**
 * Soong drush commands.
 */
class SoongCommands extends DrushCommands {

  /**
   * Execute a migration.
   *
   * @param string $migration_names
   *   Restrict to a comma-separated list of migrations (Optional).
   *
   * @command soong:migrate
   *
   * @usage soong:migrate article
   *   Execute the migrate operation for the 'article' plugin.
   */
  public function migrate($migration_names = '') {
    $this->loadTasks();
    foreach (explode(',', $migration_names) as $id) {
      // @todo: Hard-coded Task implementation
      if ($task = Task::getTask($id)) {
        $this->output()->writeln("<info>Executing $id</info>");
        $task->execute('migrate');
      } else {
        $this->output()->writeln("<error>$id not found</error>");
      }
    }
  }

  /**
   * Rollback a migration.
   *
   * @param string $migration_names
   *   Restrict to a comma-separated list of migrations (Optional).
   *
   * @command soong:rollback
   *
   * @usage soong:rollback article
   *   Execute the migrate operation for the 'article' plugin.
   */
  public function rollback($migration_names = '') {
    $this->loadTasks();
    foreach (explode(',', $migration_names) as $id) {
      // @todo: Hard-coded Task implementation
      if ($task = Task::getTask($id)) {
        $this->output()->writeln("<info>Executing $id</info>");
        $task->execute('rollback');
      } else {
        $this->output()->writeln("<error>$id not found</error>");
      }
    }
  }

  /**
   * Obtain all task configuration contained in the specified directories.
   *
   * @param array $directoryNames
   *   List of directories containing task configuration.
   */
  protected function loadTasks()
  {
    $directoryNames = [drupal_get_path('module', 'soong') . '/migrations'];
    foreach ($directoryNames as $directoryName) {
      $directory = new \RecursiveDirectoryIterator(
        $directoryName,
        \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS
      );
      $filter = new YamlRecursiveFilterIterator($directory);
      $allFiles = new \RecursiveIteratorIterator(
        $filter,
        \RecursiveIteratorIterator::SELF_FIRST
      );
      foreach ($allFiles as $file) {
        if (!$file->isDir()) {
          $configuration = Yaml::parse(file_get_contents($file->getPathname()));
          foreach ($configuration as $id => $taskConfiguration) {
            /** @var \Soong\Task\TaskInterface $taskClass */
            $taskClass = $taskConfiguration['class'];
            $taskClass::addTask($id, $taskConfiguration['configuration']);
          }
        }
      }
    }
  }

}
