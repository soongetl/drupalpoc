<?php
declare(strict_types=1);

namespace Drupal\soong\Extractor;

use Soong\Data\Record;
use Soong\Extractor\CountableExtractorBase;

/**
 * Extractor for in-memory arrays.
 *
 * @package Soong\Extractor
 */
class ArrayExtractor extends CountableExtractorBase {

  /**
   * {@inheritdoc}
   */
  public function extractAll(): iterable {
    foreach ($this->configuration['data'] as $data) {
      // @todo: Inject DataRecordInterface implementation.
      $dataObject = new Record();
      $dataObject->fromArray($data);
      yield $dataObject;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getProperties(): array {
    return array_keys($this->configuration['data'][0]);
  }
}
