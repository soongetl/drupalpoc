<?php
declare(strict_types=1);

namespace Drupal\soong\Transformer;

use Soong\Data\Property;
use Soong\Data\DataPropertyInterface;
use Soong\Transformer\TransformerInterface;

/**
 * Transformer to uppercase the first letter of the extracted data.
 *
 * @package Soong\Transformer
 */
class UcFirst implements TransformerInterface
{

  /**
   * {@inheritdoc}
   */
  public function transform(array $configuration, ?DataPropertyInterface $data) : ?DataPropertyInterface
  {
    // @todo Don't use concrete class
    return new Property(ucfirst($data->getValue()));
  }
}
