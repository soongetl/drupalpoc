<?php
declare(strict_types=1);

namespace Drupal\soong\Database;

use PDO;
use Soong\Extractor\CountableExtractorBase;

/**
 * Extractor for Drupal database API queries.
 */
class Extractor extends CountableExtractorBase {

  use DatabaseTrait;

  /**
   * {@inheritdoc}
   */
  public function extractAll(): iterable {
    // @todo: don't accept raw SQL from configuration
    $statement = $this->connection()->query($this->configuration['query'], [],
      ['fetch' => PDO::FETCH_ASSOC]);
    foreach ($statement as $row) {
      $dataRecordClass = $this->configuration['data_record_class'];
      /** @var \Soong\Data\DataRecordInterface $dataObject */
      $dataObject = new $dataRecordClass();
      $dataObject->fromArray($row);
      yield $dataObject;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getProperties(): array {
    // @todo: Identify properties from the query.
    return parent::getProperties();
  }

  // @todo implement count() to use SQL COUNT().
}
