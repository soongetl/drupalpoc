<?php
declare(strict_types=1);

namespace Drupal\soong\Database;

use Soong\Data\DataRecordInterface;
use Soong\Data\Property;
use Soong\Loader\LoaderBase;

/**
 * Loader for Drupal database API tables.
 */
class Loader extends LoaderBase {

  use DatabaseTrait;

  /**
   * {@inheritdoc}
   */
  public function load(DataRecordInterface $data): void {
    $id = $this->connection()->insert($this->configuration['table'])
      ->fields($data->toArray())
      ->execute();
    if ($id) {
      // Only applies in auto-increment case.
      $keyKeys = array_keys($this->getKeyProperties());
      $data->setProperty(reset($keyKeys), new Property($id));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getProperties(): array {
    return $this->configuration['properties'];
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyProperties(): array {
    return $this->configuration['key_properties'];
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $key): void {
    $query = $this->connection()->delete($this->configuration['table']);
    foreach ($key as $name => $value) {
      $query->condition($name, $value);
    }
    $query->execute();
  }
}
