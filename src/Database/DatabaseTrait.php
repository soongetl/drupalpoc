<?php
declare(strict_types=1);

namespace Drupal\soong\Database;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;

/**
 * Common implementation for Soong's Drupal database API implementations.
 */
trait DatabaseTrait
{

  /**
   * Gets the database connection object.
   *
   * @return \Drupal\Core\Database\Connection
   *   The database connection.
   */
  public function connection() : ?Connection {
    $key = $this->configuration['connection']['key'] ?: 'default';
    $target = $this->configuration['connection']['target'] ?: 'default';
    if (isset($this->configuration['connection']['database'])) {
      Database::addConnectionInfo($key, $target, $this->configuration['connection']['database']);
    }
    $connection = Database::getConnection($target, $key);
    return $connection;
  }

}
