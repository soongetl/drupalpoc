<?php
declare(strict_types=1);

namespace Drupal\soong\Database;

use Drupal\Core\Database\DatabaseException;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use PDO;
use Soong\KeyMap\KeyMapBase;

/**
 * Implementation of key maps using Drupal database API for storage.
 */
class KeyMap extends KeyMapBase {

  use DatabaseTrait;

  /**
   * Prefixes for database column names.
   */
  const EXTRACTED_KEY_PREFIX = 'extracted_key_';

  const LOADED_KEY_PREFIX = 'loaded_key_';

  /**
   * The database table name for the key map.
   *
   * @var string
   */
  protected $tableName;

  /**
   * Save the table name.
   *
   * @param array $configuration
   *   Configuration values keyed by configuration name.
   */
  public function __construct(array $configuration) {
    parent::__construct($configuration);
    $this->tableName = $this->configuration['table'];
  }

  /**
   * {@inheritdoc}
   */
  public function saveKeyMap(array $extractedKey, array $loadedKey): void {
    if (!$this->tableExists()) {
      $this->createTable();
    }
    $extractedKey = array_values($extractedKey);
    $loadedKey = array_values($loadedKey);
    $counter = 0;
    $fields = [];
    foreach ($this->extractedKeyColumns() as $columnName) {
      $fields[$columnName] = $extractedKey[$counter];
      $counter++;
    }
    $counter = 0;
    foreach ($this->loadedKeyColumns() as $columnName) {
      $fields[$columnName] = $loadedKey[$counter];
      $counter++;
    }
    $keys = ['hash' => $this->hashKeys($extractedKey)];
    $this->connection()->merge($this->tableName)
      ->keys($keys)
      ->fields($fields)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function lookupLoadedKey(array $extractedKey): array {
    if (!$this->tableExists()) {
      return [];
    }
    $query = $this->connection()->select($this->tableName, 'map');
    $query->condition('hash', $this->hashKeys($extractedKey));
    foreach ($this->loadedKeyColumns() as $keyName => $columnName) {
      $query->addField('map', $columnName, $keyName);
    }
    $result = $query->execute()->fetchAssoc();
    if (!$result) {
      return [];
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function lookupExtractedKeys(array $loadedKey): array {
    if (!$this->tableExists()) {
      return [];
    }
    $query = $this->connection()->select($this->tableName, 'map');
    foreach ($this->extractedKeyColumns() as $columnName) {
      $query->addField('map', $columnName);
    }
    $loadedKeys = array_values($loadedKey);
    $counter = 0;
    foreach ($this->loadedKeyColumns() as $columnName) {
      $query->condition($columnName, $loadedKeys[$counter]);
      $counter++;
    }
    $result = $query->execute()->fetchAllKeyed();
    if (!$result) {
      return [];
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $extractedKey): void {
    if (!$this->tableExists()) {
      return;
    }
    $this->connection()->delete($this->tableName)
      ->condition('hash', $this->hashKeys($extractedKey))
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function count(): int {
    if (!$this->tableExists()) {
      return 0;
    }
    try {
      $count = $this->connection()
        ->query("SELECT COUNT(*) FROM {$this->tableName}")
        ->execute()
        ->fetchField();
    } catch (DatabaseException $e) {
      print $e->getMessage();
    }
    return (int) $count;
  }

  /**
   * Create the key map table if it doesn't already exist.
   */
  protected function createTable(): void {
    if (!$this->tableExists()) {
      // Generate appropriate schema info for the map and message tables,
      // and map from the extractor field names to the map/msg field names.
      $source_id_schema = [];
      $indexes = [];
      foreach ($this->extractedKeyColumns() as $keyName => $columnName) {
        $indexes['source'][] = $columnName;
        // @todo Ideally derived from the extractor key defs
        $source_id_schema[$columnName] = [
          'type' => 'varchar',
          'length' => 255,
        ];
        $source_id_schema[$columnName]['not null'] = TRUE;
      }

      $source_ids_hash['hash'] = [
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
        'description' => 'Hash of source ids. Used as primary key',
      ];
      $fields = $source_ids_hash + $source_id_schema;

      // Add destination identifiers to map table.
      foreach ($this->loadedKeyColumns() as $keyName => $columnName) {
        // @todo Ideally derived from the loader key defs
        $fields[$columnName] = [
          'type' => 'varchar',
          'length' => 255,
        ];
        $fields[$columnName]['not null'] = FALSE;
      }

      $fields['hash'] = [
        'type' => 'varchar',
        'length' => '64',
        'not null' => FALSE,
        'description' => 'Hash of source row data, for detecting changes',
      ];
      $schema = [
        'description' => 'Mappings from extractor identifier value(s) to loader identifier value(s).',
        'fields' => $fields,
        'primary key' => ['hash'],
        'indexes' => $indexes,
      ];
      $this->connection()->schema()->createTable($this->tableName, $schema);
    }
  }

  /**
   * See if the database table exists.
   *
   * @return bool
   *   TRUE if the table exists, FALSE otherwise.
   */
  protected function tableExists(): bool {
    return $this->connection()->schema()->tableExists($this->tableName);
  }

  /**
   * Construct and return the names of the extracted key columns.
   *
   * @return array
   *   List of extracted key column names, keyed by the extracted key name.
   */
  protected function extractedKeyColumns(): array {
    $counter = 1;
    foreach (array_keys($this->configuration[self::CONFIGURATION_EXTRACTOR_KEYS]) as $keyName) {
      $columnName = self::EXTRACTED_KEY_PREFIX . $counter++;
      $result[$keyName] = $columnName;
    }
    return $result;
  }

  /**
   * Construct and return the names of the loaded key columns.
   *
   * @return array
   *   List of loaded key column names, keyed by the loaded key name.
   */
  protected function loadedKeyColumns(): array {
    $counter = 1;
    foreach (array_keys($this->configuration[self::CONFIGURATION_LOADER_KEYS]) as $keyName) {
      $columnName = self::LOADED_KEY_PREFIX . $counter++;
      $result[$keyName] = $columnName;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function iterate(): iterable {
    if ($this->tableExists()) {
      $query = $this->connection()->select($this->tableName, 'map', ['fetch' => PDO::FETCH_ASSOC]);
      foreach ($this->extractedKeyColumns() as $keyName => $columnName) {
        $query->addField('map', $columnName, $keyName);
      }
      foreach ($query->execute() as $row) {
        yield $row;
      }
    }
  }
}
